#!/bin/sh

for infile in ./sample-problems/*.in; do
    res="$(python main.py < $infile)"
    outfile="./sample-problems/$(basename $infile .in).out"
    actual_res="$(cat $outfile)"
    if [ $res = $actual_res ]; then
        echo "$(basename $infile) correct"
    else
        echo "$(basename $infile) incorrect"
    fi
done
