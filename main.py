from abc import ABC, abstractmethod
import heapq as hq
import sys


### GLOBAL CONSTANS ###
N, M, T = [int(item) for item in input().split()]
SOURCE = 0
SINK = N - 1
MAX_CAPACITY = 10**7
MAX_PATH_LENGTH = T * N


### CLASSES
class Edge(ABC):
    """ Abstract class that represents a highway between two cities. """

    def __init__(self, l: int, c: int, f: int =0):
        """ Attributes:
          * length: the length of the edge
          * capacity: the maximum flow possible through this edge
          * flow: the flow going through this edge """

        self.length = l
        self.capacity = c
        self.flow = f


    @abstractmethod
    def isResidual(self) -> bool:
        """ Should be implemented by the child classes of Edge. It
        should return True if the edge is residual and False
        otherwise. """
        
        pass


    def inc_flow(self, f: int) -> bool:
        """ Increment the flow of the edge by f (f can be negative)
        if possible and return True if successful and False 
        otherwise. """

        if 0 <= self.flow + f and self.flow + f <= self.capacity:
            self.flow += f
            return True
        return False
    

    def __lt__(self, o):
        """ Used for sorting edges. """

        return self.length < o.length


    def get_rest_flow(self) -> int:
        """" Return the unused flow of the edge. """

        return self.capacity - self.flow


    def has_rest_flow(self) -> int:
        """" Return True if the edge has unused flow and False
        otherwise. """

        return self.get_rest_flow() > 0


class ResidualEdge(Edge):
    """ The Residual edge is the inverse of the NormalEdge and has a
    flow of capacity at initiation. """

    def __init__(self, l: int, c: int):
        Edge.__init__(self, l, c, c)


    def isResidual(self) -> bool:
        return True


class NormalEdge(Edge):
    """ A normal edge. """

    def __init__(self, l: int, c: int):
        Edge.__init__(self, l, c)


    def isResidual(self) -> bool:
        return False


class Graph:
    def __init__(self, N: int, M: int):
        """ Attributes:
          * N: the number of vertices in the graph
          * M: the number of edges in the graph
          * graph: adjacency list vertex connections
          * edges: NxN adjacency matrix of a list of Edges, from city
                x to city y. There may be multiple edges from x to y """

        self.N = N
        self.M = M
        self.graph, \
        self.edges = self.__read_graph(N, M)


    def __read_graph(self, N: int, M: int) -> tuple:
        """ Read the lines from stdin and create the adjacency list 
        to represent the graph and the adjacency matrix representing
        the residual graph. 
        
        @param:
          * N: the number of cities in the graph
          * M: the number of edge in the graph 
          
        @return the adjacency list representing the graph and the
            adjacency matrix representing the residual graph. """
        
        graph = [[] for i in range(N)]
        edges = [[False]*N for i in range(N)]
        for i in range(M):
            u, v, l, c = [int(item) for item in input().split()]  # from, to, length, capacity
            nedge = NormalEdge(l, c)
            redge = ResidualEdge(l, c)
            if not edges[u][v]:
                graph[u].append(v)
                graph[v].append(u)
                edges[u][v] = []
                edges[v][u] = []
            edges[u][v].append(nedge)
            edges[v][u].append(redge)
        return graph, edges
    

    def __relax(self, u: int, v: int, min_road: int, len_suv: int, Q: list) -> None:
        """ Relax edge u to v and update the queue Q if appropriate. 
        
        @param:
          * u: from vertex u
          * v: to vertex v
          * min_road: the road index between u and v with shortest
              length and positive rest flow
          * len_suv: the shortest length from SOURCE to u to v
          * Q: heap of tuples (length_from_source_to_vertex, vertex,
              parent_in_shortest_path) """

        for i in range(len(Q)):
            len_sx, x, px, rx = Q[i]
            if x == v and len_sx > len_suv:
                Q[i] = (len_suv, v, u, min_road)
                return


    def __get_shortest_road(self, u: int, v: int) -> int:
        """ Find the shortest road from u to v with positive rest
        flow. 

        @param:
          * u: from vertex u
          * v: to vertex v

        @return the index of the shortest road from u to v with
            positive rest flow. If no road exists with positive
            rest flow return -1. """

        edge = self.edges[u][v]
        min_road = None
        for road_nr in range(len(edge)):
            
            if min_road != None:
                if edge[min_road].length > edge[road_nr].length and edge[road_nr].has_rest_flow():
                    min_road = road_nr
            else:
                if edge[road_nr].has_rest_flow():
                    min_road = road_nr

        return min_road if min_road != None else -1


    def __dijkstra(self) -> list:
        """ Perform a slightly modified Dijkstra's shorttest path
        algorithm to find the path with the shorstest length.

        @return: list of vertices that make up the shortest path 
            if a path is found and None otherwise. """

        # Q := (length_from_source_to_vertex, vertex, parent_vertex, road_nr)
        Q = [(MAX_PATH_LENGTH, i, -1, -1) for i in range(N)]  # Q is a min-heap
        Q[0] = (0, SOURCE, -1, -1)  # heap is still in tact
        parents = [(-1, -1)] * N  # (parent_vertex, road_number)
        while Q:
            l, u, p, r = hq.heappop(Q)  # length, vertex, parent_vertex, road_nr
            parents[u] = (p, r)
            if u == SINK:  # shortest path is found
                return parents
            for v in self.graph[u]:
                min_road = self.__get_shortest_road(u, v)
                if min_road >= 0:
                    len_suv = l + self.edges[u][v][min_road].length
                    if len_suv <= T:
                        self.__relax(u, v, min_road, len_suv, Q)
            hq.heapify(Q)
        return parents


    def __find_shortest_path(self) -> tuple:
        """ Find the short path using Dijkstra and return the path
        along with the maximum flow of the path. 
        NOTE: the order of the shortest_path is from SINK to SOURCE. 
        
        @return: a tuple containing the maximum flow along the
            shortest path and the list vertices that make up the
            shortest path. """

        parents = self.__dijkstra()

        if parents[-1][0] < SOURCE:  # There is no path
            return (None, None)

        max_flow = MAX_CAPACITY
        v = SINK
        shortest_path = []
        shortest_path.append((v, -1))

        while True:
            u, min_road_uv = parents[v]
            max_flow = min(max_flow, self.edges[u][v][min_road_uv].get_rest_flow())
            shortest_path.append((u, min_road_uv))
            if u == SOURCE:
                break
            v = u

        return (max_flow, shortest_path)


    def ford_fulkerson(self) -> None:
        """ Run the ford_fulkerson max flow algorithm on the graph,
        after the function is done self.edges will be the residual
        graph. """

        while True:
            max_flow, shortest_path = self.__find_shortest_path()
            if not shortest_path:  # quit if there is no more paths
                break

            for i in range(len(shortest_path)-1):
                v = shortest_path[i][0]
                u = shortest_path[i+1][0]
                min_road_uv = shortest_path[i+1][1]

                if not (self.edges[u][v][min_road_uv].inc_flow(max_flow) and \
                        self.edges[v][u][min_road_uv].inc_flow(-max_flow)):
                    print("This should never be printed.", file=sys.stderr)
                    sys.exit(1)


    def __find_all_residual_paths(self, u: int =SINK, road_nr: int =-1, max_flow_su: int =MAX_CAPACITY, length_su: int =0) -> list:
        """ Perform a Depth First Search on the graph to find all
        paths from SINK to SOURCE using only residual edges. Return
        the max flow and length of each path.
        
        @param:
          * u: the current vertex to be explored
          * road_nr: the road index used to get to u
          * max_flow_su: that maximum flow possible on the path being 
                explored (SOURCE to u)
          * length_su: the length of the path being eplored (SOURCE to u) 
        
        @return:
          a list of tuples where each tuple consists of:
            0: the length of the path
            1: the max flow of the path """

        # quit early if the path is useless for the final result
        if length_su > T or max_flow_su <= 0:
            return []
        if u == SOURCE:  # The SOURCE has been reached
            return [(length_su, max_flow_su)]
        paths = []
        for v in self.graph[u]:
            edge = self.edges[u][v]
            for road_nr_uv in range(len(edge)):
                road = edge[road_nr_uv]
                if road.isResidual():
                    length_sv = length_su + road.length
                    flow_sv = min(max_flow_su, road.get_rest_flow())
                    for partial_path in self.__find_all_residual_paths(v, road_nr_uv, flow_sv, length_sv):
                        if partial_path:
                            paths.append(partial_path)

        return paths


    def solve(self):
        """ Compute the maximum number of trucks that can ride from
        SOURCE to SINK within T hours. """

        res = 0
        paths = self.__find_all_residual_paths()
        
        for length, flow in paths:
            res += (T-length+1) * flow
        return res


if __name__ == '__main__':
    g = Graph(N, M)
    g.ford_fulkerson()
    print(g.solve())
