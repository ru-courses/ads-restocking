\documentclass[a4paper]{article}

\usepackage{minted}            %environment for adding code blocks
\usepackage{booktabs}           % nicer tables

%\usepackage{tikz}              % environment for drawing things
\usepackage{float}              % for the 'H' positioning of figures
\usepackage{a4wide}             % To decrease the margins and allow more text on a page.
\usepackage{graphicx}           % To deal with including pictures.
\usepackage{color}              % To add color.
\usepackage{enumerate}          % To provide a little bit more functionality than with 
                                % LaTeX's default enumerate environment.
\usepackage{array}              % To provide a little bit more functionality than with 
                                % LaTeX's default array environment.
\usepackage{listings}           % for the formatting of code blocks
\usepackage[american]{babel}
\usepackage{amsmath,amssymb}    % better equations
%\usepackage{amsthm}             % proof environment

\lstset{                        % for the boxing of code blocks
    basicstyle=\footnotesize, 
    frame=single,
    breaklines=true,
    numbers=left,
    tabsize=2,
}

% Exercise headers
% \newcommand{\exercise}[2]{\subsection*{Exercise #1}{#2}}
% \newcommand{\exerciseenum}[2]{\subsection*{Exercise #1}{\begin{enumerate}[a)]#2\end{enumerate}}}
% Short cut for set creation
\newcommand{\set}[1]{\ensuremath{\left\{{#1}\right\}}}
\newcommand{\setbuild}[2]{\ensuremath{\set{{#1}\mid{#2}}}}
% Short cuts for well known mathematical sets
\newcommand{\NN}{\ensuremath{\mathbb{N}}}
\newcommand{\ZZ}{\ensuremath{\mathbb{Z}}}
\newcommand{\QQ}{\ensuremath{\mathbb{Q}}}
\newcommand{\RR}{\ensuremath{\mathbb{R}}}
% Short cuts for calligraphic letters
\newcommand{\CO}{\ensuremath{\mathcal{O}}}
% An the next command gives a shorthand for the power set of a given set.
\newcommand{\sol}[1]{\underline{\underline{#1}}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{Algorithms And Data Structures\\Assignment: Restocking}

\author{Bryan Rinders, s1060340\\Grzegorz Roguszczak, s1107470}

\begin{document}
\maketitle 

\section{Algorithm explanation}


From the problem description it is very clear that this is a max flow
problem with some extra conditions, the maximum length of a path and
the possibility for multiple roads between two cities. The very high
level overview of our solution is:
\begin{enumerate}[1.]
\item
  \begin{enumerate}[a)]
  \item Reading the input
  \item Creating the graph from the input.  
  \end{enumerate}
\item Apply a max flow algorithm on the graph to get the residual graph.
\item Use the residual graph to compute the final answer. 
\end{enumerate}

\subsection{Creating the graph}
Before reading the input however we need to define two classes, one to
represent an edge between two cities and one to represent the
graph. An edge is further sub-categorized as a 'normal edge' and a
'residual edge'. A normal edge is an edge initialized as read from the
input. A residual edge is the edge is the inverse of its normal edge
counter part. The residual edge is used during the max flow algorithm
to 'undo' the flow of a normal edge. The graph class is the collection
of all edge (normal and residual). The graph class has the graph both
as a adjacency list as well as a adjacency matrix. The reason for this
is that is gives both the speed of access to a cities normal edge as
well as its residual edges and vice versa. Due to the fact that there
could be multiple roads between two cities cell in the adjacency
matrix is a list of all the edges between those two cities.
With these definition reading the input is trivial.

\subsection{Max flow}
The problem with finding the max flow of a graph is how do you find
augmenting paths. Given the constraints of the problem description,
more precisely the time limit T, using Edmonds-Karp which makes use of
breath first search will not work because the number of edges in a
path is not correlated to the length of a path. And these 'shortest
paths' in terms of edges that have a length longer than T will then
use flow that could be used by paths with length less than T. A
solution to this is to use Dijkstra's shortest path algorithm to find
augmenting paths. Dijkstra has been modified to work with multiple
edges between two cities and will only use paths with a length less
than T and a positive max flow. When Dijkstra has to choose between
multiple edges between two cities then it will always choose the edge
with the least length that has not yet maxed out its flow to capacity.

\subsection{The final solution}
To compute the final solution we first need to find the all the paths
that have a positive flow. We can do this by looking at the residual
edges. If we perform a depth first search only on residual edges and
keep track of the length and the flow that is used for each path then
given a path with length $l$ and flow $f$ the number of trucks $s$
that can use this path and get to the store in time is:
\[
  s = (T-l+1) \cdot f
\]
where $T$ is the time limit. \\
If $P$ contains every residual path found by the DFS then:
\[
  S = \sum_{i \in P} (T-l_i+1) \cdot f_i
\]
computes the total number of trucks that can ride from the
distribution centre to the supermarket within $T$ hours. Where $l_i$
and $f_i$ is the length and flow of path $i$ respectively.


\section{Correctness}
The correctness of the algorithm will be proven by stepping backwards
through the algorithm and proving each assumption made at every step. \\

\subsection{The final solution}
\subsubsection{Compute the total number of trucks}
Assume a set $P$. $P$ contains the max flows and lengths
of each path that can be taken by the trucks. Let $f_i$ and $l_i$ be
the max flow and length of path $i \in P$. The maximum number of
trucks that can drive on $i$ from distribution to supermarket is
$(T-l_i+1) \cdot f_i$, where $T$ is the time limit. $(T-l_i+1)$
represents how the number of times trucks can leave the distribution
centre and still arrive on time at the supermarket, multiplying this
by the number of trucks that can maximally drive the path results in
$S$, the total number of trucks that can reach the supermarket in time
using this path. Hence $S$ is computed as:
\[
S = \sum_{i \in P} (T-l_i+1) \cdot f_i
\]

\subsubsection{Find the paths with flow}
To get the set $P$ assume $G$ is the residual graph that
contains the max flows only for paths with length less than $T$. This
can be done with a Depth First Search (DFS) which keeps track of the
length and the max flow of the paths and relies on the correctness of
DFS.

\subsection{Max flow}
To get the residual graph $G$ that contains the max flows only for
paths with length less than $T$ from the graph $G'$ Ford-Fulkerson
must run on $G'$. Ford-Fulkerson does not specify how to find
augmenting paths and because $G$ must only contain flow paths with
length less than $T$ Dijkstra's shortest path algorithm can be
used. Therefore the correctness of finding $G$ relies on the
correctness of Ford-Fulkerson and Dijkstra.

\subsection{Creating the graph}
% Add stuff here
To create the graph and the initial residual graph $G'$ all lines from
the input must be read and for each line $G'$ must contain the normal
edge and its residual edge counter part in.

\section{Complexity}
% Add stuff here
Operations of setting constants run in linear time. 

During the creation of the graph, the function \verb+__read_graph+ has
the time complexity of $\CO(N)$ for setting up the empty graph,
$\CO(N^2)$ for setting up the adjacency matrix, named \verb+edges+,
then $\CO(M)$ for the loop that adds the edges to \verb+graph+ and
\verb+edges+. Because $N^2 > M$ the total complexity of initializing
the graph is $\CO(N^2)$. \\

Then, we call to function \verb+ford_fulkerson+, which calls to
function \verb+__find_shortest_path+, which calls
\verb+dijkstra+. \verb+Dijkstra+ is, as the name suggests, a
Dijkstra's shortest path algorithm, so it takes $\CO(M\log(N))$. The
while loop in the \verb+__find_shortest_path+ function in worst case
scenario will go through all the edges, so it will be $O(M)$ if all
the roads make one long path. Total for \verb+__find_shortest_path+ is
then $O(M + M\log(N)) = \CO(M\log(N))$. \verb+ford_fulkerson+ executes
as long as there's a shortest path possible, therefore it would
execute the loop that calls \verb+__find_shortest_path+ at worst $M$
times. So, the complexity of \verb+ford_fulkerson+ would be
$\CO(M^2\log(N))$. \\

Finally, the function \verb+solve+ is called on the graph, which calls
\verb+__find_all_residual_paths+ and also a for loop that executes at
worst $M$ times. \verb+__find_all_residual_paths+ is a DFS, and
therefore $\CO(N+M)$. Hence \verb+solve+ has complexity $\CO(N+M)$.

The whole project would have the time complexity of $\CO(N+M + +
M^2\log(N) + N + M) = \CO(M^2\log(N))$.

\end{document}
